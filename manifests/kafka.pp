class kafka::kafka {

  package { 'ha-kafka': ensure  =>  "latest"}

  service { 'kafka':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require    => File['/opt/kafka/config/server.properties']
  }

  file { '/opt/kafka/config/server.properties':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('kafka/server.properties.erb'),
    require => Service['kafka-zookeeper'],
  }

  file { '/opt/kafka/config/consumer.properties':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('kafka/consumer.properties.erb'),
    require => Service['kafka-zookeeper'],
  }

  file { '/opt/kafka/config/producer.properties':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('kafka/producer.properties.erb'),
    require => Service['kafka-zookeeper'],
  }


}