class kafka::zookeeper {

  service { 'kafka-zookeeper':
    ensure      => running,
    enable      => true,
    hasstatus   => true,
    hasrestart  => true,
    require     => File['/tmp/zookeeper/myid']
  }

  file { '/opt/kafka/config/zookeeper.properties':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('kafka/zookeeper.properties.erb'),
    require => Package['ha-kafka'],
  }

  file { '/tmp/zookeeper/myid':
    ensure  => 'present',
    owner   => 'zookeeper',
    group   => 'zookeeper',
    mode    => '0644',
    content => template('kafka/myid.erb'),
    require => File['/opt/kafka/config/zookeeper.properties'],
  }


}